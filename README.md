# ros_gitlab_cicd

Create a docerized ROS application

# Features
*  [Colcon](https://colcon.readthedocs.io/en/released/) - colcon is a command line tool to improve the workflow of building, testing and using multiple software packages. It automates the process, handles the ordering and sets up the environment to use the packages.
*  [vcstool](https://github.com/dirk-thomas/vcstool) to import source packages - vcstool is a version control system (VCS) tool, designed to make working with multiple repositories easier.
*  Production-grade docker image of ROS application 

# How to use
include it in your ROS package's [gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/):
```
include:
  - project: 'robotwebservices/ros_gitlab_cicd'
    ref: develop
    file: '/ros_gitlab_cicd.yml'

build: 
  extends: .build
  variables:
    INPUT_IMAGE: registry.gitlab.com/robotwebservices/docker_images/ros-foundation:melodic-develop

```
The resulting docker image is commited to your [gitlab repo's docker registry](https://docs.gitlab.com/ee/user/project/container_registry.html)
### REPOS FILE
The default .repos file is located in the root folder: `<root folder name>.repos.`


# Example
An example of using ros_gitlab_cicd can be found in the [vegetable_picking_example demonstration docker application](https://gitlab.com/robotwebservices/vegetable_picking_example)
